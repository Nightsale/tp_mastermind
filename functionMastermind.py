import random
#definission de la fonction test_combi_valid
def test_combi_valid(combinaison, alphabet):
    """
    |test_combi_valid(combinaison,alphabet)
    |type(combinaison)=list
    |type(alphabet)=list
    |------------------------------------------------------------------------------------
    |Verifie si la combinaison est valide et est present dans l'alphabet
    |-----------------------------------------------------------------------------------
    |return:
    |   True== combinaison valide
    """
    for i in range(len(combinaison)):
        if combinaison[i] not in alphabet:
            print("Error : " + combinaison[i] + " n'est pas une valeur valide")
            return False
        for h in range(len(combinaison)):
            if combinaison[i] == combinaison[h] and i != h:
                print("Error : votre liste contient un doublon")
                return False
    return True
#definission de la fonction input_combi
def input_combi(alphabet, n):
    combinaison=[]
    x=0
    i=1
    for _ in range(n):
        x=str(input('Choisis la lettre de ta combinaison:'))
        combinaison.append(x)
        i+=1
    return combinaison

#definission de la fonction print_combi
def print_combi(combinaison):
    x=combinaison
    return f"[{' | '.join(x)}]"

def compare_id_nb(combinaison,reference):
    i=0
    res=0
    for _ in range (len(combinaison)):
        if reference[i]==combinaison[i]:
            res+=1
        i+=1
    return res
def compare_diff_nb(combinaison,reference):
    i=0
    res=0
    tmp=combinaison
    for _ in range(len(combinaison)):
        if combinaison[i] in reference and combinaison[i] != reference[i]:
            res+=1
        i+=1
    return res


#definission de la fonction generate_combi
def generate_combi(alphabet,n):
    """
    |generate(alphabet,n)
    |renvoi une combinaison de lettre aleatoire parmis l'alphabet
    |:param alphabet:
    |type(alphabet) == list
    |:param n:
    |type(n) == int
    |:return:
    |combinaison sous forme de liste
    """
    assert type(n)==int
    assert type(alphabet)==list
    longeur=len(alphabet)-1
    valeur=[]
    for _ in range (n):
        x=random.randint(0,longeur)
        while x in valeur:
            x=random.randint(0,longeur)
        valeur.append(x)
    combinaison=[]
    for i in valeur:
        combinaison.append(alphabet[i])
    reference=combinaison
    return reference

def play_v1(alphabet,n,coupMax):
    coup=0
    print("  __________________________________________",'\n',
          "|Alphabet:", alphabet,"|", '\n',
          "|Nombre d'element a deviner:", n,"           |",'\n',
          "|Coup maximal:", coupMax, " coups","                 |",'\n',
          " __________________________________________")
    ref = generate_combi(alphabet, n)
    print("le jeu peut commencer")
    while coup != coupMax:
        combinaison= input_combi(alphabet, n)
        while test_combi_valid(combinaison, alphabet)==False:
            combinaison = input_combi(alphabet, n)
        print("Voici ta combinaison:",print_combi(combinaison))
        printCombi=print_combi(combinaison)
        compareId = compare_id_nb(combinaison, ref)
        comparediff = compare_diff_nb(combinaison, ref)
        if combinaison==ref:
            return True
        else:
            print(
            "-----------------------------------------------------------------------------------------------------------------------------------",
            '\n', "Dommage la combinaison n'est pas bonne.", "Il y a", compareId,
            "element qui sont bien placer et ", comparediff, "elements qui sont mal placer", '\n', "Il te reste ",
            coupMax-coup-1, "coup", '\n',
            "-----------------------------------------------------------------------------------------------------------------------------------")
            coup+=1
    return False

#Partie play_v2 et play_v3

def compare_combi_mask(combinaison, masquer):
    for i in masquer:
        assert type(i) == bool
    i = 0
    val = 0
    combinaisonDef = []
    for _ in range(len(combinaison)):
        if masquer[i] == True:
            val = combinaison[i]
            combinaisonDef.append(val)
        else:
            combinaisonDef.append("*")
        i += 1
    return print_combi(combinaisonDef)


def compare_id_pos(combinaison, reference):
    a = 0
    masquer = []
    for i in combinaison:
        if reference[a] == i:
            masquer.append(True)
        else:
            masquer.append(False)
        a += 1
    return masquer


def generate_combi_repet(alphabet, n):
    """
    |generate(alphabet,n)
    |renvoi une combinaison de lettre aleatoire parmis l'alphabet
    |:param alphabet:
    |type(alphabet) == list
    |:param n:
    |type(n) == int
    |:return:
    |combinaison sous forme de liste
    """
    assert type(n) == int
    assert type(alphabet) == list
    longeur = len(alphabet) - 1
    valeur = []
    for _ in range(n):
        x = random.randint(0, longeur)
        valeur.append(x)
    combinaison = []
    for i in valeur:
        combinaison.append(alphabet[i])
    reference = combinaison
    return reference


def test_combi_valid_repet(combinaison, alphabet):
    for i in range(len(combinaison)):
        if combinaison[i] not in alphabet:
            print("Error : " + combinaison[i] + " n'est pas une valeur valide")
            return False
    return True


def compare_diff_nb_repet(combinaison, reference):
    a = 0
    res = 0
    for i in combinaison:
        if reference[a] != i:
            res += 1
        a += 1
    return res


def play_v2(alphabet, n, coupMax):
    coup = 0
    print("  __________________________________________", '\n',
          "|Alphabet:", alphabet, "|", '\n',
          "|Nombre d'element a deviner:", n, "           |", '\n',
          "|Coup maximal:", coupMax, " coups", "                 |", '\n',
          " __________________________________________")
    ref = generate_combi(alphabet, n)
    print("le jeu peut commencer")
    while coup != coupMax:
        combinaison = input_combi(alphabet, n)
        while test_combi_valid(combinaison, alphabet) == False:
            combinaison = input_combi(alphabet, n)
        print("Voici ta combinaison: ", print_combi(combinaison))
        printCombi = print_combi(combinaison)
        compareId = compare_id_nb(combinaison, ref)
        comparediff = compare_diff_nb(combinaison, ref)
        masquer = compare_id_pos(combinaison, ref)
        compareList = compare_combi_mask(combinaison, masquer)
        if combinaison == ref:
            return True
        else:
            print(
                "-----------------------------------------------------------------------------------------------------------------------------------",
                '\n', "Dommage la combinaison n'est pas bonne.", "Il y a", compareId,
                "element qui sont bien placer", compareList, "et", comparediff, "elements qui sont mal placer", '\n',
                "Il te reste ", coupMax - coup - 1
                , "coup", '\n',
                "-----------------------------------------------------------------------------------------------------------------------------------")
        coup += 1
    return False

#Partie play_v3


def play_v3(alphabet, n, coupMax):
    coup = 0
    print("  __________________________________________", '\n',
          "|Alphabet:", alphabet, "|", '\n',
          "|Nombre d'element a deviner:", n, "           |", '\n',
          "|Coup maximal:", coupMax, " coups", "                 |", '\n',
          " __________________________________________")
    ref = generate_combi_repet(alphabet, n)
    print("le jeu peut commencer")
    while coup != coupMax:
        combinaison = input_combi(alphabet, n)
        while test_combi_valid_repet(combinaison, alphabet) == False:
            combinaison = input_combi(alphabet, n)
        print("Voici ta combinaison:", print_combi(combinaison))
        compare_id = compare_id_nb(combinaison, ref)
        compare_diff = compare_diff_nb_repet(combinaison, ref)
        if combinaison == ref:
            return True
        else:
            print(
                "-----------------------------------------------------------------------------------------------------------------------------------",
                '\n',"Dommage la combinaison n'est pas bonne.", "Il y a", compare_id,
                "element qui sont bien placer et ", compare_diff, "elements qui sont mal placer", '\n', "Il te reste ",
                coupMax - coup - 1, "coup", '\n',
                "-----------------------------------------------------------------------------------------------------------------------------------")
            coup += 1
    return False
