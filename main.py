from functionMastermind import *
def menu():
    """
    Utilisation:
        Fonction servant a afficher un menu pour l'utilisateur afin de modifier c'est paramaretres de jeu
    Variable d'entrée:
        Cette fonctionne ne requiert pas de Variable d'entrée
    Variable de sortie:
        Cette fonction retournera soit:
            -print("Bravo tu as gagné")
                soit
            -print("A bientôt.")
    """
    print("Bienvenue dans le menu de Mastermind.\n Tu pourras ici changer l'alphabet, le nombre d'éléments a deviner ainsi que le nombre de coup.")
    alphabet = ["r", "j", "v", "o", "n", "m"]
    n = 4
    coupMax = 12
    recomencer=True
    while recomencer==True:
        print("Les paramaretres actuel sont:\n -Alphabet=",alphabet,"\n -Nombre d'élements a deviner(nb elements)=",n,"\n -Nombre de coups max(CoupMax)=",coupMax)
        param=str(input("Quelle parametre veut tu changer.(Si tu ne veux rien changer appui sur entrée)\n"))
        nb=x=0
        if param=="alphabet" or param=="Alphabet":
            alphabet=[]
            nb=int(input("combien de valeur contiendra ton alphabet ?"))
            assert nb>=0
            for _ in range(nb):
                x=str(input("Choisis une lettre"))
                assert x[:-1]==""
                alphabet.append(x)
        elif param=="CoupMax" or param=="coupmax" or param=="Coupmax":
            coupMax=int(input("Choisi un nombre de coup max"))
        elif param=="nb elements":
            n=int(input("Choisi un le nombre d'éléments a deviner'"))
        elif param=="":
            print("le paramètre choisi n'existe pas ou est mal orthographier")

        reco=str(input("Veut-tu changer d'autre paramètre ?(oui/non)"))
        if reco=="oui" or reco=="Oui":
            recomencer=True
        elif reco=="non" or reco=="Non":
            return alphabet, n, coupMax
def play():
    """
        Utilisation:
        Fonction servant a lier toute les fonction entre elle pour jouer au jeu Mastermind
    Variable d'entrée:
        Cette fonctionne requiert pas de Variable d'entrée
    Variable de sortie:
        Cette fonction sortira trois Variable alphabet,n,coupMax sous forme d'un tuple incrementer dans cette ordre
    """
    #Val defaut
    alphabet = ["r", "j", "v", "o", "n", "m"]
    n = 4
    coupMax = 12
    #
    print("Bienvenue sur le jeu Mastermind.\nLe but du jeu est dans un nombre de coup maximal de trouver la combinaison generer par l'ordinateur.")
    res=str(input("Veux tu acceder au paramètre du jeu ?(oui/non)"))
    if res=="oui":
        val=menu()
        alphabet=val[0]
        n=val[1]
        coupMax=val[2]
    print("Il existe 3 modes de jeu :\n1.Sans répétion\n2.Sans répétition anvec une aide\n3.Avec Répétition")
    res=int(input("Quelle mode choisis-tu ?"))
    if res==1:
       res=play_v1(alphabet,n,coupMax)
    elif res==2:
        res=play_v2(alphabet,n,coupMax)
    elif res==3:
        res=play_v3(alphabet,n,coupMax)
    if res>0:
        print("Bravo tu as gagné")
    else:
        print("dommage tu as perdus")
    rejou=str(input("Veux tu rejouer ? (oui/non)"))
    if rejou=="oui":
        play()
    else:
        return print("A bientôt.")
play()

